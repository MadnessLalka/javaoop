package AbstractClass.FirstLesson;

public class Student extends Learner {
    @Override
    public String toString() {
        return getLastName() + ";" + super.getMark1() + ";" + super.getMark2() + ";" + collegeName + ";" + group;
    }

    private String collegeName;
    private String group;
    private static int basicScholarshipAmount;

    public Student() {

    }

    public Student(String lastName, int mark1, int mark2, String collegeName, String group) {
        super(lastName, mark1, mark2);
        this.collegeName = collegeName;
        this.group = group;
    }


    @Override
    int totalDurationOfHolidaysPerYear() {
        return 74;
    }

    public int calculateScholarship() {
        if (super.averageMark() / 2 >= 9) {
            return (int) (basicScholarshipAmount * 1.6);
        } else if (super.averageMark() / 2 >= 8) {
            return (int) (basicScholarshipAmount * 1.4);
        } else if (super.averageMark() / 2 >= 6) {
            return (int) (basicScholarshipAmount * 1.2);
        } else if (super.averageMark() / 2 >= 5) {
            return basicScholarshipAmount;
        } else {
            return 0;
        }
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public static int getBasicScholarshipAmount() {
        return basicScholarshipAmount;
    }

    public void setBasicScholarshipAmount(int basicScholarshipAmount) {
        Student.basicScholarshipAmount = basicScholarshipAmount;
    }

}
