package AbstractClass.FirstLesson;

public class Schoolboy extends Learner {
    public Schoolboy() {
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getClassName() {
        return className;
    }

    @Override
    public String toString() {
        return getLastName() + ";" + super.getMark1() + ";" + super.getMark2()+ ";"+ school + ";" + className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Schoolboy(String lastName, int mark1, int mark2, String school, String className) {
        super(lastName, mark1, mark2);
        this.school = school;
        this.className = className;
    }

    private String school;
    private String className;

    @Override
    int totalDurationOfHolidaysPerYear() {
        return 124;
    }
}
