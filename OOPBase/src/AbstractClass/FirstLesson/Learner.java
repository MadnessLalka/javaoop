package AbstractClass.FirstLesson;

public abstract class Learner {
    public Learner(String lastName, int mark1, int mark2) {
        LastName = lastName;
        this.mark1 = mark1;
        this.mark2 = mark2;
    }
    public Learner(){

    }

//    @Override
//    public String toString() {
//        return getLastName() + ";" + mark1 + ";" + mark2 + ";";
//    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public int getMark1() {
        return mark1;
    }

    public void setMark1(int mark1) {
        this.mark1 = mark1;
    }

    public int getMark2() {
        return mark2;
    }

    public void setMark2(int mark2) {
        this.mark2 = mark2;
    }

    private String LastName;
    private int mark1;
    private int mark2;

    public int averageMark() {
        return getMark1() + getMark2();
    }

    abstract int totalDurationOfHolidaysPerYear();
}

