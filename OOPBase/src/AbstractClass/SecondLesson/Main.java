package AbstractClass.SecondLesson;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int quantityPets = scanner.nextInt();
        scanner.nextLine();

        Pet[] petsArray = new Pet[quantityPets];

        for (int i = 0; i < petsArray.length; i++) {
            String[] newPet = scanner.nextLine().split(" ");
            if(newPet[0].equals("1")){
                petsArray[i] = new Dog(newPet[1], Integer.parseInt(newPet[2]), newPet[3]);

            } else if (newPet[0].equals("2")) {
                petsArray[i] = new Cat(newPet[1], Integer.parseInt(newPet[2]), newPet[3]);

            }
        }

        for (Pet p: petsArray){
            System.out.print(p.getClass().getSimpleName() + " " + p.getName() + ":");
            p.getNoise();
        }
        System.out.println();

        for (Pet p: petsArray){
            if (p instanceof Cat){
                System.out.println(p.getName()+ ";"+ p.getAge() +";"+ ((Cat) p).getColor());
            }
        }

    }
}
