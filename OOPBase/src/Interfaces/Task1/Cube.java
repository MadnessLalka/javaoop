package Interfaces.Task1;

public class Cube implements IShape {
    private double h;

    public Cube() {
    }

    public Cube(double h) {
        this.h = h;
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    @Override
    public double computeSquare() {
        return 6 * Math.pow(h, 2);
    }
}
