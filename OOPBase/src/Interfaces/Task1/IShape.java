package Interfaces.Task1;

public interface IShape {
    double computeSquare();
}
