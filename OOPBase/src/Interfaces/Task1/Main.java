package Interfaces.Task1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);

        int quantityShapes = scanner.nextInt();
        scanner.nextLine();

        IShape[] shapesArray = new IShape[quantityShapes];

        for (int i = 0; i < shapesArray.length; i++) {
            String[] dataShape = scanner.nextLine().split(" ");
            if(dataShape[0].equals("1")){
                shapesArray[i] = new Rectangle(Double.parseDouble(dataShape[1]), Double.parseDouble(dataShape[2]));

            } else if (dataShape[0].equals("2")) {
                shapesArray[i] = new Cube(Double.parseDouble(dataShape[1]));
            }
        }

        double maxRectangleSquare = 0;
        double maxA = 0, maxB = 0;

        for (IShape IS: shapesArray){
            System.out.printf(String.format("%.2f", IS.computeSquare()) + " ");
            if (IS instanceof Rectangle && IS.computeSquare() > maxRectangleSquare && IS.computeSquare() != maxRectangleSquare) {
                maxRectangleSquare = IS.computeSquare();
                maxA = ((Rectangle) IS).getA();
                maxB = ((Rectangle) IS).getB();
            }
        }
        System.out.println();

        if (maxRectangleSquare == 0){
            System.out.println("ERROR");
        } else {
            System.out.printf("Прямоугольник сторона 1 = %.1f, сторона 2 = %.1f Площадь = %.2f", maxA, maxB, maxRectangleSquare);
        }

    }
}
