package Interfaces.Task2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);

        int sizeTransportArray = scanner.nextInt();
        scanner.nextLine();

        Transport[] transportArray = new Transport[sizeTransportArray];

        for (int i = 0; i < transportArray.length; i++) {
            String[] transport = scanner.nextLine().split(" ");

            if (transport[0].equals("1")){
                transportArray[i] = new Lorry(Double.parseDouble(transport[1]));
            } else if(transport[0].equals("2")){
                transportArray[i] = new Ship(Double.parseDouble(transport[1]));
            }
        }

        double currentWeight = scanner.nextDouble();
//        scanner.nextLine();

        for (Transport t: transportArray){
            if(t.canCarry(currentWeight)){
                t.add(currentWeight);
            }
            System.out.println(t);
        }

    }
}
