package Interfaces.Task2;

public class Lorry implements Transport {
    private double weight;
    private double maximumWeight;

    public Lorry(double weight) {
        maximumWeight = weight;
    }

    public Lorry() {
    }

    public double getMaximumWeight() {
        return maximumWeight;
    }

    public void setMaximumWeight(double maximumWeight) {
        this.maximumWeight = maximumWeight;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Lorry{" +
                "maxWeight=" + getMaximumWeight() +
                ", totalWeight=" + getTotalWeight() +
                '}';
    }

    @Override
    public boolean canCarry(double weight) {
        if (this.getMaximumWeight() >= weight) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void add(double weight) {
        this.setWeight(weight);
    }

    @Override
    public double getTotalWeight() {
        return this.weight;
    }
}
