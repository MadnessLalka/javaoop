package Exceptions.Task3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Luggage economStandart = new Luggage(10);

        double weightLuggage = scanner.nextDouble();

        try {
            System.out.println("Надо пройти регистрацию на рейс");
            economStandart.checkLuggage(weightLuggage);
            System.out.println("Доплата за вес не нужна");
        } catch (HeavyLuggageException ex){
            System.out.println(ex.getLocalizedMessage());
            System.out.println("Нужно оплатить превышение веса по тарифу");
        } finally {
            System.out.println("Регистрация пройдена");
        }

    }
}
