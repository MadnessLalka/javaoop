package Exceptions.Task3;

public class HeavyLuggageException extends Exception{
    public HeavyLuggageException() {
        super("Превышение веса багажа");
    }
}
