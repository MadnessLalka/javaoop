package Exceptions.Task2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);

        Parking parking = new Parking((byte) 10);

        byte carQuantity = scanner.nextByte();

        try {
            System.out.println("Приехали на парковку");
            parking.check(carQuantity);
            System.out.println("Припарковались");
        } catch (TooManyCarsException ex) {
            System.out.println(ex.getLocalizedMessage());
            System.out.println("Припарковались в другом месте");
        } finally {
            System.out.println("Выходим из машин");
        }

    }
}
