package Exceptions.Task1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String foodDescription = scanner.nextLine();

        Cook cook = new Cook();

        try {
            cook.turnOvenOn();
            cook.bake(foodDescription);
            System.out.println("Блюдо готово");
        } catch (BakingException ex) {
            System.out.println("Блюдо полностью испортилось: " + ex.getLocalizedMessage());
        } finally {
            cook.turnOvenOff();
        }
    }
}
