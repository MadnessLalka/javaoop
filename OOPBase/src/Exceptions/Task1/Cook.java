package Exceptions.Task1;

public class Cook {
    private String product;

    public Cook() {
    }

    public Cook(String product) {
        this.product = product;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public void turnOvenOn(){
        System.out.println("Включаю плиту");
    }

    public void turnOvenOff(){
        System.out.println("Выключаю плиту");
    }

    public void bake(String product) throws BakingException{
            System.out.println("Готовлю блюдо");
            if (product.equals("Прогорклое масло") || product.equals("Низкосортная мука")) throw new BakingException(product);
            System.out.println("Конец приготовления блюда");
    }
}
